package ca.gc.psc.bug.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author gchamber
 *         On 2016/02/11.
 */

@Entity
@Data
public class EntityA {
    @Id
    private Long id;

    private String name;

    @ManyToOne
    private EntityB b;
}
