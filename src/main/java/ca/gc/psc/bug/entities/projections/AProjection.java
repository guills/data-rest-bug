package ca.gc.psc.bug.entities.projections;

import ca.gc.psc.bug.entities.EntityA;
import ca.gc.psc.bug.entities.EntityB;
import org.springframework.data.rest.core.config.Projection;

/**
 * @author gchamber
 *         On 2016/02/11.
 */
@Projection(types = EntityA.class, name = "aProjection")
public interface AProjection {
    Long getId();
    String getName();
    EntityB getB();
}
