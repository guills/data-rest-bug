package ca.gc.psc.bug.entities.projections;

import ca.gc.psc.bug.entities.EntityB;
import ca.gc.psc.bug.entities.EntityC;
import org.springframework.data.rest.core.config.Projection;

/**
 * @author gchamber
 *         On 2016/02/11.
 */
@Projection(types = EntityB.class, name = "bProjection")
public interface BProjection {
    Long getId();
    String getName();
    EntityC getC();
}
