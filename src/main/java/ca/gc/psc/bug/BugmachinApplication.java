package ca.gc.psc.bug;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BugmachinApplication {

	public static void main(String[] args) {
		SpringApplication.run(BugmachinApplication.class, args);
	}
}
