package ca.gc.psc.bug.initializer;

import ca.gc.psc.bug.repositories.ARestRepository;
import ca.gc.psc.bug.repositories.BRestRepository;
import ca.gc.psc.bug.repositories.CRestRepository;
import ca.gc.psc.bug.entities.EntityA;
import ca.gc.psc.bug.entities.EntityB;
import ca.gc.psc.bug.entities.EntityC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author gchamber
 *         On 2016/02/11.
 */
@Service
public class AEntityInitializer {

    @Autowired
    public AEntityInitializer(ARestRepository aRepo, BRestRepository bRepo, CRestRepository cRepo) {
        if (aRepo.count() > 0)
        {
            return;
        }
        EntityC c = new EntityC();
        c.setId(1L);
        c.setName("Foo");

        EntityB b = new EntityB();
        b.setId(2L);
        b.setName("Bar");
        b.setC(c);

        EntityA a = new EntityA();
        a.setId(3L);
        a.setName("Foobar");
        a.setB(b);

        cRepo.save(c);
        bRepo.save(b);
        aRepo.save(a);
    }
}
