package ca.gc.psc.bug.repositories;

import ca.gc.psc.bug.entities.projections.BProjection;
import ca.gc.psc.bug.entities.EntityB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author gchamber
 *         On 2016/02/11.
 */
@RepositoryRestResource(excerptProjection = BProjection.class)
public interface BRestRepository extends JpaRepository<EntityB, Long> {

}
