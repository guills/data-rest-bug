package ca.gc.psc.bug.repositories;

import ca.gc.psc.bug.entities.projections.AProjection;
import ca.gc.psc.bug.entities.EntityA;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author gchamber
 *         On 2016/02/11.
 */
@RepositoryRestResource(excerptProjection = AProjection.class)
public interface ARestRepository extends JpaRepository<EntityA, Long> {

}
