package ca.gc.psc.bug.repositories;

import ca.gc.psc.bug.entities.EntityC;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author gchamber
 *         On 2016/02/11.
 */
@RepositoryRestResource
public interface CRestRepository extends JpaRepository<EntityC, Long> {

}
